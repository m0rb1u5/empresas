//
//  SecuritySettings.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import SwiftUI
import Combine
import Security

/// Variaveis de segurança
class SecuritySettings: ObservableObject {
    // MARK: - Properties

    /// Se o usuário foi carregado
    @Published var isUserLoaded: Bool = false

    /// Crrega as variáveis do keychain se tem
    init() {
        if let email = UserDefaults.standard.object(forKey: "email") as? String {
            let query: [String: Any] = [
                kSecClass as String: kSecClassGenericPassword,
                kSecAttrAccount as String: email,
                kSecMatchLimit as String: kSecMatchLimitOne,
                kSecReturnAttributes as String: true,
                kSecReturnData as String: true,
            ]

            var item: CFTypeRef?

            if SecItemCopyMatching(query as CFDictionary, &item) == noErr {
                if let existingItem = item as? [String: Any],
                   let _ = existingItem[kSecAttrAccount as String] as? String,
                   let passwordData = existingItem[kSecValueData as String] as? Data,
                   let _ = String(data: passwordData, encoding: .utf8) {
                    self.isUserLoaded = true
                }
            }
        }
    }

    /// Relaiza logout. Para isso deleta as variáveis de keychain e de usuário
    func logout() {
        if let email = UserDefaults.standard.object(forKey: "email") as? String {
            UserDefaults.standard.removeObject(forKey: "email")
            UserDefaults.standard.removeObject(forKey: "client")
            UserDefaults.standard.removeObject(forKey: "accessToken")
            UserDefaults.standard.removeObject(forKey: "uid")

            let query: [String: Any] = [
                kSecClass as String: kSecClassGenericPassword,
                kSecAttrAccount as String: email,
            ]

            if SecItemDelete(query as CFDictionary) == noErr {
                self.isUserLoaded = false
            }
        }
    }
}

