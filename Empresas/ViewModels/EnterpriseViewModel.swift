//
//  EnterpriseViewModel.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import SwiftUI
import Combine
import Security

/// View model que trata as empresas
class EnterpriseViewModel: ObservableObject {
    // MARK: - Properties

    /// Se tá carregando na tela de inicio
    @Published var isLoading: Bool = false
    /// Se vai ser mostrado um erro na tela
    @Published var showError: Bool = false
    /// Descrição do erro se é mostrado
    @Published var errorDescription: String = ""
    /// Texto de busca
    @Published var searchText: String = "" {
        didSet {
            self.searchEnterprises()
        }
    }
    /// As empresas resultantes
    @Published var enterprises: [Enterprise] = []

    private var cancellables = [AnyCancellable]()
    /// Cliente do API de network
    let apiClient = APIClient(baseURL: AppConstants.baseURL)

    // MARK: - Methods

    /// Realiza a requisição para procurar empresas com o valor da busca
    func searchEnterprises() {
        guard !self.searchText.isEmpty else {
            self.enterprises = []
            return
        }

        self.isLoading = true

        self.apiClient.dispatch(EnterpriseRequest(searchText: self.searchText))
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                self.isLoading = false
                switch completion {
                case .finished:
                    debugPrint("EnterpriseViewModel.searchEnterprises: searchEnterprises")
                case .failure(let error):
                    self.showError = true
                    self.errorDescription = "\(error)"
                }
            }, receiveValue: { value in
                debugPrint("EnterpriseViewModel.searchEnterprises: enterprises.count=\(value.enterprises.count)")
                self.enterprises = value.enterprises
            })
            .store(in: &self.cancellables)
    }

    /// Usado para saber se vai mostrar a tela de empresas não achadas
    /// - Returns: True se não foi achada nenhuma empresa
    func showNotFoundView() -> Bool {
        return !self.searchText.isEmpty && self.enterprises.isEmpty && !self.isLoading
    }
}
