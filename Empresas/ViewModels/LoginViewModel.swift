//
//  LoginViewModel.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import SwiftUI
import Combine
import Security

/// View model de login
class LoginViewModel: ObservableObject {
    // MARK: - Properties

    /// email
    @Published var email: String = ""
    /// Se o email é válido
    @Published var isValidEmail: Bool = true
    /// Senha
    @Published var password: String = ""
    /// Se tá carregando uma requisição
    @Published var isLoading: Bool = false
    /// Se vai ser mostrado um erro na tela
    @Published var showError: Bool = false
    /// Descrição do erro se é mostrado
    @Published var errorDescription: String = ""
    /// Se o login tá completado
    @Published var completedLogin: Bool = false

    private var cancellables = [AnyCancellable]()
    /// Cliente do API de network
    let apiClient = APIClient(baseURL: AppConstants.baseURL)

    // MARK: - Methods

    /// Método para tentar logarse
    func signIn() {
        self.isLoading = true

        let attributes: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: self.email,
            kSecValueData as String: self.password.data(using: .utf8) ?? self.password,
        ]

        self.apiClient.dispatch(SignInRequest(email: self.email, password: self.password))
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                self.isLoading = false
                switch completion {
                case .finished:
                    debugPrint("LoginViewModel.signIn: OK")
                case .failure(let error):
                    self.showError = true
                    self.errorDescription = "\(error)"
                    UserDefaults.standard.removeObject(forKey: "client")
                    UserDefaults.standard.removeObject(forKey: "accessToken")
                    UserDefaults.standard.removeObject(forKey: "uid")
                }
            }, receiveValue: { value in
                debugPrint("LoginViewModel.signIn: \(value)")

                if value.success, SecItemAdd(attributes as CFDictionary, nil) == noErr {
                    UserDefaults.standard.set(self.email, forKey: "email")
                    self.completedLogin = true
                }
                else {
                    self.showError = true
                    self.errorDescription = "keychain error"
                }
            })
            .store(in: &self.cancellables)
    }

    /// Método usado para validar se um email é válido
    func calculateIfIsValidEmail() {
        if self.email.count > 100 {
            self.isValidEmail = false
        }

        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        self.isValidEmail = emailPredicate.evaluate(with: self.email)
    }

    /// Se o formulario é válido
    /// - Returns: True se o formulario tá correto
    func isValidForm() -> Bool {
        return !self.email.isEmpty && self.isValidEmail && !self.password.isEmpty
    }
}
