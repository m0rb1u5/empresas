//
//  Enterprise.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import Foundation
import Combine

/// Empresa
struct Enterprise: Codable, Identifiable {
    var id: Int
    /// Email da empresa
    var email_enterprise: String?
    var facebook: String?
    var twitter: String?
    var linkedin: String?
    /// Celular da empresa
    var phone: String?
    /// Se sou dono da empresa?
    var own_enterprise: Bool
    /// Nome
    var enterprise_name: String
    /// Foto
    var photo: String
    /// Descrição
    var description: String
    /// Cidade
    var city: String
    /// Pais
    var country: String
    /// Valor da empresa
    var value: Int
    /// Preço compartido
    var share_price: Double
    /// Tipo de empresa
    var enterprise_type: EnterpriseType
}
