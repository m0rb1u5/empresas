//
//  EnterpriseType.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import Foundation
import Combine

/// Tipo da empresa
struct EnterpriseType: Codable {
    var id: Int
    /// Nome
    var enterprise_type_name: String
}
