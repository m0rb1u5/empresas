//
//  PortFolio.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import Foundation
import Combine

/// Portfolio do investor
struct PortFolio: Codable {
    /// Número de empresas investidas
    var enterprises_number: Int
}
