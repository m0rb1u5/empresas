//
//  EnterpriseRequest.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import Foundation
import Combine

/// Request para procurar empresas por nome
struct EnterpriseRequest: Request {
    // MARK: - Request
    
    typealias ReturnType = EnterpriseResponse
    
    var method: HTTPMethod = .get
    var path: String = "/enterprises"
    var queryParams: [String : String]?
    var body: [String : Any]?
    var contentType: String = "application/json"

    // MARK: - Init

    /// Inicializador da estrutura
    /// - Parameter searchText: Texto de busca de empresas
    init(searchText: String) {
        self.queryParams = ["name": searchText]
    }
}
