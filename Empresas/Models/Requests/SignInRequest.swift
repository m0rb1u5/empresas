//
//  SignInRequest.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import Foundation
import Combine

/// Request para nos logar
struct SignInRequest: Request {
    // MARK: - Request
    
    typealias ReturnType = SignInResponse

    var method: HTTPMethod = .post
    var path: String = "/users/auth/sign_in"
    var queryParams: [String : String]?
    var body: [String : Any]?
    var contentType: String = "application/json"

    // MARK: - Init

    /// Inicializador da estrutura
    /// - Parameters:
    ///   - email: email
    ///   - password: senha
    init(email: String, password: String) {
        self.body = ["email": email, "password": password]
    }
}
