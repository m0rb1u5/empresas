//
//  Investor.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import Foundation
import Combine

struct Investor: Codable {
    var id: Int
    /// Nome do Investor
    var investor_name: String
    var email: String
    /// Cidade
    var city: String
    /// Pais
    var country: String
    /// Conta
    var balance: Double
    /// Foto
    var photo: String?
    var portfolio: PortFolio
    /// Valor do portfolio atual
    var portfolio_value: Double
    /// Se é o primeiro acesso
    var first_access: Bool
    /// Se é super usuário?
    var super_angel: Bool
}
