//
//  SignInResponse.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import Foundation
import Combine

/// Response de logeo
struct SignInResponse: Codable {
    /// Investidor
    var investor: Investor
    /// Empresa do investidor
    var enterprise: String?
    /// Se teve sucesso o login
    var success: Bool
}
