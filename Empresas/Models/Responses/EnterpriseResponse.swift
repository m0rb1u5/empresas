//
//  EnterpriseResponse.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import Foundation
import Combine

/// Response da consulta de empresas por nome
struct EnterpriseResponse: Codable {
    /// Empresas resultantes
    var enterprises: [Enterprise]
}
