//
//  AppConstants.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import Foundation
import SwiftUI

/// Constantes do App
class AppConstants {
    /// URL principal
    static let principalURL: String = "https://empresas.ioasys.com.br/"
    /// URL base
    static let baseURL: String = "\(AppConstants.principalURL)api/v1"

    /// Cores do gradiente de fundos da tela de login
    static var backgroundGradientColors: [Color] = [
        .black,
        Color(red: 33/255, green: 0, blue: 0),
        Color(red: 88/255, green: 39/255, blue: 70/255),
        Color(red: 33/255, green: 0, blue: 0),
        .black]
    /// Cores da celda de empresas na tela principal
    static var backgroundCellColors: [Color] = [
        Color(red: 96/255, green: 31/255, blue: 56/255),
        Color(red: 136/255, green: 93/255, blue: 124/255)]
}
