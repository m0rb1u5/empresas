//
//  Request.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import Foundation
import Combine

/// Protocolo dos requests
public protocol Request {
    /// Path que vem depois do URL
    var path: String { get set }
    /// Methodo de consulta
    var method: HTTPMethod { get }
    /// Tipo de conteúdo da requisição
    var contentType: String { get }
    /// Parâmetros de consulta
    var queryParams: [String: String]? { get set }
    /// Body do request se tem
    var body: [String: Any]? { get set }
    /// Headers do request se tem
    var headers: [String: String]? { get }

    /// Resposta do request
    associatedtype ReturnType: Codable
}

/// Valores default
extension Request {
    var method: String { return HTTPMethod.get.rawValue }
    var contentType: String { return "application/json" }
    var queryParams: [String: String]? { return nil }
    var body: [String: Any]? { return nil }
    var headers: [String: String]? { return nil }
}

extension Request {
    /// Serializa o corpo da mensagem. Usado para a autenticação
    /// - Parameter params: Parâmetros
    /// - Returns: JSON encodado
    private func requestBodyFrom(params: [String: Any]?) -> Data? {
        guard let params = params else { return nil }
        guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return nil
        }
        return httpBody
    }
    /// Transforma um request em um URLRequest usado pelo URLSession
    /// - Parameter baseURL: URL base
    /// - Returns: O request já formatado
    func asURLRequest(baseURL: String) -> URLRequest? {
        guard var urlComponents = URLComponents(string: baseURL) else {
            return nil
        }
        
        urlComponents.path = "\(urlComponents.path)\(path)"

        var queryItems: [URLQueryItem] = []

        if let queryParams = self.queryParams {
            for (queryKey, queryValue) in queryParams {
                queryItems.append(URLQueryItem(name: queryKey, value: queryValue))
            }
        }

        urlComponents.queryItems = queryItems

        guard let finalURL = urlComponents.url else { return nil }
        var request = URLRequest(url: finalURL)

        request.setValue("\(self.contentType); charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("\(self.contentType); charset=utf-8", forHTTPHeaderField: "Accept")

        if let client = UserDefaults.standard.object(forKey: "client") as? String {
            request.setValue(client, forHTTPHeaderField: "client")
        }

        if let accessToken = UserDefaults.standard.object(forKey: "accessToken") as? String {
            request.setValue(accessToken, forHTTPHeaderField: "access-token")
        }

        if let uid = UserDefaults.standard.object(forKey: "uid") as? String {
            request.setValue(uid, forHTTPHeaderField: "uid")
        }

        request.httpMethod = method.rawValue
        request.httpBody = requestBodyFrom(params: body)
        request.allHTTPHeaderFields = headers
        return request
    }
}
