//
//  HomeView.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import SwiftUI
import AlertToast

/// Tela inicial
struct HomeView: View {
    // MARK: - Properties

    /// Variaveis de segurança
    @EnvironmentObject var securitySettings: SecuritySettings
    /// View model das empresas
    @StateObject var viewModel = EnterpriseViewModel()

    /// Colunas do grid
    var columns: [GridItem] = [
        GridItem(.flexible()),
        GridItem(.flexible()),
    ]

    // MARK: - View

    var body: some View {
        NavigationView {
            VStack {
                if self.viewModel.showNotFoundView() {
                    EnterpriseNotFoundView()
                }
                else {
                    ScrollView {
                        LazyVGrid(columns: self.columns, spacing: 16) {
                            ForEach(self.viewModel.enterprises) { enterprise in
                                NavigationLink(destination: EnterpriseDetailView(enterprise: enterprise)) {
                                    EnterpriseCellView(enterprise: enterprise)
                                }
                            }
                        }
                    }
                }
            }
                .searchable(text: self.$viewModel.searchText, prompt: "Buscar...")
                .navigationTitle("Empresas")
                .toolbar {
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button {
                            self.securitySettings.logout()
                        } label: {
                            Text("Logout")
                        }
                    }
                }
        }
        .toast(isPresenting: self.$viewModel.isLoading) {
            AlertToast(type: .loading)
        }
        .toast(isPresenting: self.$viewModel.showError, duration: 2, tapToDismiss: true) {
            AlertToast(displayMode: .hud, type: .error(.red), title: "Erro", subTitle: self.viewModel.errorDescription)
        }

    }
}

/// Preview da tela inicial
struct HomeView_Previews: PreviewProvider {
    // MARK: - View

    static var previews: some View {
        HomeView()
    }
}
