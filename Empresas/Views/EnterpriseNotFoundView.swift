//
//  EnterpriseNotFoundView.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import SwiftUI

/// Empresas não encontradas
struct EnterpriseNotFoundView: View {
    // MARK: - View

    var body: some View {
        VStack(spacing: 16) {
            Image("enterpriseNotFound")
                .resizable()
                .scaledToFill()
                .frame(width: 200, height: 200, alignment: .center)
                .accessibilityIdentifier("enterpriseNotFound")

            Text("Empresa não encontrada")
                .font(.heeboRegular16)
                .foregroundColor(.neutral2)
        }
    }
}

/// Preview das empresas não encontradas
struct EnterpriseNotFoundView_Previews: PreviewProvider {
    // MARK: - View

    static var previews: some View {
        EnterpriseNotFoundView()
    }
}
