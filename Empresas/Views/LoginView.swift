//
//  LoginView.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 04/11/21.
//

import SwiftUI
import AlertToast

/// Tela de login
struct LoginView: View {
    // MARK: - Properties

    /// Variaveis de segurança
    @EnvironmentObject var securitySettings: SecuritySettings

    /// View model da tela de login
    @StateObject var viewModel = LoginViewModel()

    /// Se vai ser mostrado o password na tela
    @State var isSecured: Bool = true
    /// O campo que tá em foco nesse momento
    @FocusState private var focusedField: Field?

    /// Usado para os campos de foco
    enum Field {
        case email
        case password
    }

    // MARK: - View

    /// Formulario de login
    var forms: some View {
        VStack(alignment: .leading, spacing: 0) {
            Text("Digite seus dados para continuar.")
                .font(.gilroySemiBold16)
                .foregroundColor(.primary)

            Text(self.viewModel.email.isEmpty ? "" : "Email")
                .font(.robotoMedium12)
                .foregroundColor(self.viewModel.isValidEmail ? .neutral : .statusError)
                .padding(.top, 24)

            TextField("Email", text: self.$viewModel.email, onEditingChanged: { isChanged in
                if !isChanged {
                    self.viewModel.calculateIfIsValidEmail()
                }
            })
                .foregroundColor(.neutral2)
                .textFieldStyle(.roundedBorder)
                .focused(self.$focusedField, equals: .email)
                .textContentType(.emailAddress)
                .submitLabel(.next)

            Text(self.viewModel.isValidEmail ? "" : "Endereço de email inválido")
                .font(.robotoMedium12)
                .foregroundColor(.statusError)

            Text(self.viewModel.password.isEmpty ? "" : "Senha")
                .font(.robotoMedium12)
                .foregroundColor(.neutral)
                .padding(.top, 8)

            ZStack(alignment: .trailing) {
                if self.isSecured {
                    SecureField("Senha", text: self.$viewModel.password)
                        .foregroundColor(.neutral2)
                        .textFieldStyle(.roundedBorder)
                        .focused(self.$focusedField, equals: .password)
                        .textContentType(.password)
                        .submitLabel(.join)
                }
                else {
                    TextField("Senha", text: self.$viewModel.password)
                        .foregroundColor(.neutral2)
                        .textFieldStyle(.roundedBorder)
                        .focused(self.$focusedField, equals: .password)
                        .textContentType(.password)
                        .submitLabel(.join)
                }

                Button {
                    self.isSecured.toggle()
                } label: {
                    Image(systemName: self.isSecured ? "eye.slash" : "eye")
                        .accentColor(.gray)
                }
            }
        }
        .onSubmit {
            switch self.focusedField {
            case .email:
                self.focusedField = .password
            case .password:
                debugPrint("LoginView.forms: onSubmit")
            case .none:
                debugPrint("LoginView.forms: erro no onSubmit")
            }
        }
    }

    /// Botão para fazer a requisição de autenticação
    var submitButton: some View {
        Button {
            self.focusedField = nil
            if self.viewModel.isValidForm() {
                self.viewModel.signIn()
            }
        } label: {
            Text("ENTRAR")
                .padding()
                .frame(maxWidth: .infinity)
                .background(self.viewModel.isValidForm() ? Color.primary : Color.neutral)
                .foregroundColor(.white)
                .clipShape(Capsule())
        }
        .padding(.top, 22)
    }

    var body: some View {
        VStack(spacing: 0) {
            Spacer()

            ZStack(alignment: .top) {
                HStack(spacing: 0) {
                    Spacer()

                    Image("back_logo")
                        .foregroundColor(.white)
                }

                VStack(spacing: 0) {
                    HStack(spacing: 0) {
                        VStack(alignment: .leading, spacing: 0) {
                            Text("Boas vindas,")
                                .font(.gilroyBold40)

                            Text("Você está no Empresas.")
                                .font(.robotoLight24)
                        }
                        .foregroundColor(.white)
                        .padding(.leading, 24.0)

                        Spacer()
                    }
                    
                    VStack(alignment: .leading, spacing: 0) {
                        VStack(spacing: 0) {
                            self.forms

                            self.submitButton
                        }
                        .padding(.vertical, 24)
                        .padding(.horizontal, 24)
                    }
                    .background(Color.white)
                    .padding(.top, 31)
                }
                .padding(.top, 60)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(
            RadialGradient(colors: AppConstants.backgroundGradientColors, center: .topLeading, startRadius: 10, endRadius: 800)
        )
        .toast(isPresenting: self.$viewModel.isLoading) {
            AlertToast(type: .loading)
        }
        .toast(isPresenting: self.$viewModel.showError, duration: 2, tapToDismiss: true) {
            AlertToast(displayMode: .hud, type: .error(.red), title: "Erro", subTitle: self.viewModel.errorDescription)
        }
        .toast(isPresenting: self.$viewModel.completedLogin, duration: 2, tapToDismiss: true, alert: {
            AlertToast(displayMode: .hud, type: .complete(.green))
        }, completion: {
            self.securitySettings.isUserLoaded = true
        })
    }
}

/// Preview da tela de login
struct LoginView_Previews: PreviewProvider {
    // MARK: - View

    static var previews: some View {
        LoginView()
    }
}
