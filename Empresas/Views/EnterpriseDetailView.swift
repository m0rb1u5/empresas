//
//  EnterpriseDetailView.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import SwiftUI

/// Detalhe da empresa
struct EnterpriseDetailView: View {
    // MARK: - Properties

    /// Modo para fazer dismiss
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>

    /// Empresa
    var enterprise: Enterprise

    // MARK: - View

    var body: some View {
        GeometryReader { geo in
            ScrollView {
                VStack(spacing: 31) {
                    AsyncImage(url: URL(string: "\(AppConstants.principalURL)\(enterprise.photo)"), scale: 1.0) { imagePhase in
                        switch imagePhase {
                        case .empty:
                            ProgressView()
                                .frame(width: geo.size.width, height: 244)
                        case .success(let image):
                            image.resizable()
                                .scaledToFill()
                                .frame(width: geo.size.width, height: 244)
                                .clipped()
                                .cornerRadius(15, corners: [.bottomLeft, .bottomRight])
                        case .failure(let err):
                            Text("Não foi possível carregar a imagem\n\(err.localizedDescription)")
                                .font(.gilroyRegular14)
                                .foregroundColor(.statusError)
                                .lineLimit(4)
                                .frame(width: geo.size.width, height: 244)
                        @unknown default:
                            EmptyView()
                                .frame(width: geo.size.width, height: 244)
                        }
                    }

                    Text("\t\(self.enterprise.description)")
                        .multilineTextAlignment(.leading)
                        .fixedSize(horizontal: false, vertical: true)
                        .font(.gilroyRegular24)
                        .foregroundColor(.neutral2)
                        .padding(.horizontal, 24.0)
                        .accessibilityIdentifier("enterpriseDescription")
                }
            }
        }
        .navigationBarBackButtonHidden(true)
        .toolbar {
            ToolbarItem(placement: .navigationBarLeading) {
                Button {
                    self.mode.wrappedValue.dismiss()
                } label: {
                    Image(systemName: "arrow.left")
                    .foregroundColor(.white)
                }
            }

            ToolbarItem(placement: .principal) {
                VStack {
                    Text(self.enterprise.enterprise_name)
                        .font(.gilroyBold24)
                        .foregroundColor(.white)

                    Text("\(self.enterprise.enterprise_type.enterprise_type_name) • \(self.enterprise.city) • \(self.enterprise.country)")
                        .font(.gilroyRegular16)
                        .foregroundColor(.white)
                }
            }
        }
        .navigationBarColor(UIColor(red: 91/255, green: 41/255, blue: 58/255, alpha: 1.0))
    }
}

/// Preview do detalhe da empresa
struct EnterpriseDetailView_Previews: PreviewProvider {
    // MARK: - View

    static var previews: some View {
        EnterpriseDetailView(enterprise: Enterprise(id: 1, own_enterprise: false, enterprise_name: "Empresa exemplo", photo: "", description: "Descrição da empresa", city: "Porto Alegre", country: "Brasil", value: 1, share_price: 10.00, enterprise_type: EnterpriseType(id: 1, enterprise_type_name: "Restaurante")))
    }
}
