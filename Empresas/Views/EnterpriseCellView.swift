//
//  EnterpriseCellView.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import SwiftUI

/// Celda de uma empresa na coleção de dados
struct EnterpriseCellView: View {
    // MARK: - Properties
    
    /// Empresa
    var enterprise: Enterprise

    // MARK: - View
    
    var body: some View {
        ZStack(alignment: .top) {
            VStack(spacing: 0) {
                Rectangle()
                    .frame(width: 152, height: 32, alignment: .center)
                    .foregroundColor(.white)

                Rectangle()
                    .fill(LinearGradient(colors: AppConstants.backgroundCellColors, startPoint: .leading, endPoint: .trailing))
                    .frame(width: 152, height: 74, alignment: .center)
                    .cornerRadius(15, corners: [.topLeft, .topRight])

                ZStack {
                    Rectangle()
                        .fill(.white)
                        .frame(width: 152, height: 33, alignment: .center)
                        .cornerRadius(15, corners: [.bottomLeft, .bottomRight])
                        .shadow(color: .neutral, radius: 3)

                    Text(self.enterprise.enterprise_name)
                        .font(.gilroySemiBold14)
                        .minimumScaleFactor(0.05)
                        .foregroundColor(.neutral2)
                        .frame(width: 152, height: 33, alignment: .center)
                }
            }
            AsyncImage(url: URL(string: "\(AppConstants.principalURL)\(self.enterprise.photo)"), scale: 1.0) { imagePhase in
                switch imagePhase {
                case .empty:
                    ProgressView()
                case .success(let image):
                    image.resizable()
                        .scaledToFill()
                        .frame(width: 88, height: 106, alignment: .center)
                        .clipped()
                        .cornerRadius(15, corners: [.topLeft, .topRight])
                case .failure(let err):
                    Text("Não foi possível carregar a imagem\n\(err.localizedDescription)")
                        .font(.gilroyRegular14)
                        .foregroundColor(.statusError)
                        .lineLimit(4)
                @unknown default:
                    EmptyView()
                }
            }
        }
    }
}

/// Preview da celda de uma empresa na coleção de dados
struct EnterpriseCellView_Previews: PreviewProvider {
    // MARK: - View

    static var previews: some View {
        EnterpriseCellView(enterprise: Enterprise(id: 1, own_enterprise: false, enterprise_name: "Empresa exemplo", photo: "", description: "Descrição da empresa", city: "Porto Alegre", country: "Brasil", value: 1, share_price: 10.00, enterprise_type: EnterpriseType(id: 1, enterprise_type_name: "Restaurante")))
    }
}
