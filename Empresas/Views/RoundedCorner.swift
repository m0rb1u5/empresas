//
//  RoundedCorner.swift
//  Empresas
//
//  Created by Juan Carlos Carbajal Ipenza on 08/11/21.
//

import SwiftUI
import Foundation

/// Criado para fazer redondos os cantos das views
struct RoundedCorner: Shape {
    // MARK: - Properties

    /// Radio desejado
    var radius: CGFloat = .infinity
    /// Canto a ser modificado
    var corners: UIRectCorner = .allCorners

    // MARK: - Shape

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}
