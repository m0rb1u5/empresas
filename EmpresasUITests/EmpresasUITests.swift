//
//  EmpresasUITests.swift
//  EmpresasUITests
//
//  Created by Juan Carlos Carbajal Ipenza on 04/11/21.
//

import XCTest

class EmpresasUITests: XCTestCase {
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    /// Testa que as credenciais não estão corretas
    func testEmailVerication() throws {
        let app = XCUIApplication()
        app.launch()

        if app.buttons["Logout"].exists {
            app.buttons["Logout"].tap()
        }

        app.textFields["Email"].tap()
        app.textFields["Email"].typeText("teste@gmaom")
        app.secureTextFields["Senha"].tap()
        app.secureTextFields["Senha"].typeText("1234")

        app.buttons["ENTRAR"].tap()

        XCTAssertTrue(app.staticTexts["Endereço de email inválido"].waitForExistence(timeout: 2))
    }

    /// Testa que as credenciais não estão corretas
    func testIncorrectCredentials() throws {
        let app = XCUIApplication()
        app.launch()

        if app.buttons["Logout"].exists {
            app.buttons["Logout"].tap()
        }

        app.textFields["Email"].tap()
        app.textFields["Email"].typeText("teste@gmail.com")
        app.secureTextFields["Senha"].tap()
        app.secureTextFields["Senha"].typeText("1234")

        app.buttons["ENTRAR"].tap()

        sleep(5)

        XCTAssertTrue(app.buttons["ENTRAR"].waitForExistence(timeout: 2))
    }

    /// Testa que as credenciais estão corretas
    func testCorrectCredentials() throws {
        let app = XCUIApplication()
        app.launch()

        if app.buttons["Logout"].exists {
            app.buttons["Logout"].tap()
        }

        app.textFields["Email"].tap()
        app.textFields["Email"].typeText("testeapple@ioasys.com.br")
        app.secureTextFields["Senha"].tap()
        app.secureTextFields["Senha"].typeText("12341234")

        app.buttons["ENTRAR"].tap()

        sleep(5)

        XCTAssertFalse(app.buttons["ENTRAR"].waitForExistence(timeout: 2))
    }

    /// Testa se uma busca não foi encontrada
    func testNotFoundSearch() throws {
        let app = XCUIApplication()
        app.launch()

        if !app.buttons["Logout"].exists {
            app.textFields["Email"].tap()
            app.textFields["Email"].typeText("testeapple@ioasys.com.br")
            app.secureTextFields["Senha"].tap()
            app.secureTextFields["Senha"].typeText("12341234")

            app.buttons["ENTRAR"].tap()

            sleep(5)
        }

        app.searchFields["Buscar..."].tap()
        app.searchFields["Buscar..."].typeText("Aa")

        XCTAssertTrue(app.images["enterpriseNotFound"].waitForExistence(timeout: 2))
    }

    /// Testa se uma busca teve sucesso
    func testSucessSearch() throws {
        let app = XCUIApplication()
        app.launch()

        if !app.buttons["Logout"].exists {
            app.textFields["Email"].tap()
            app.textFields["Email"].typeText("testeapple@ioasys.com.br")
            app.secureTextFields["Senha"].tap()
            app.secureTextFields["Senha"].typeText("12341234")

            app.buttons["ENTRAR"].tap()

            sleep(5)
        }

        app.searchFields["Buscar..."].tap()
        app.searchFields["Buscar..."].typeText("A")

        sleep(2)

        XCTAssertTrue(app.images.count > 2)
    }

    /// Testa se foi possível abrir os detalhes de uma empresa
    func testOpenEnterpriseDetail() throws {
        let app = XCUIApplication()
        app.launch()

        if !app.buttons["Logout"].exists {
            app.textFields["Email"].tap()
            app.textFields["Email"].typeText("testeapple@ioasys.com.br")
            app.secureTextFields["Senha"].tap()
            app.secureTextFields["Senha"].typeText("12341234")

            app.buttons["ENTRAR"].tap()

            sleep(5)
        }

        app.searchFields["Buscar..."].tap()
        app.searchFields["Buscar..."].typeText("A")

        sleep(2)
        app.images.firstMatch.tap()

        XCTAssertTrue(app.staticTexts["enterpriseDescription"].waitForExistence(timeout: 5))
    }

    /// Teste de performance
    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
