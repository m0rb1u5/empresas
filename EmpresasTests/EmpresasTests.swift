//
//  EmpresasTests.swift
//  EmpresasTests
//
//  Created by Juan Carlos Carbajal Ipenza on 04/11/21.
//

import XCTest
import Combine
@testable import Empresas

class EmpresasTests: XCTestCase {
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    /// Testa se pode passar um email inválido
    func testInvalidEmails() throws {
        let mocks = ["nolocalpart.com",
                     "test@example.com test",
                     "user  name@example.com",
                     "user   name@example.com",
                     "example@example@example.co.uk",
                     "(test_exampel@example.fr",
                     "example(example)example@example.co.uk",
                     ".example@localhost",
                     "ex\\ample@localhost",
                     "example@local\\host",
                     "example@localhost.",
                     "user name@example.com",
                     "username@ example . com",
                     "example@(fake.com",
                     "example@(fake.com",
                     "username@example,com",
                     "usern,ame@example.com",
                     "user[na]me@example.com",
                     "\"\"\"@iana.org",
                     "\"\\\"@iana.org",
                     "\"test\"test@iana.org",
                     "\"test\"\"test\"@iana.org",
                     "\"test\".\"test\"@iana.org",
                     "\"test\".test@iana.org",
                     "\r\ntest@iana.org",
                     "\r\n test@iana.org",
                     "\r\n \r\ntest@iana.org",
                     "\r\n \r\ntest@iana.org",
                     "\r\n \r\n test@iana.org",
                     "test@iana.org \r\n",
                     "test@iana.org \r\n ",
                     "test@iana.org \r\n \r\n",
                     "test@iana.org \r\n\r\n",
                     "test@iana.org  \r\n\r\n ",
                     "test@iana/icann.org",
                     "test@foo;bar.com",
                     "comment)example@example.com",
                     "comment(example))@example.com",
                     "example@example)comment.com",
                     "example@example(comment)).com",
                     "example@1.2.3.4",
                     "example@IPv6:1:2:3:4:5:6:7:8",
                     "exam(ple@exam).ple",
                     "example@(example))comment.com"]

        let loginViewModel = LoginViewModel()

        for mock in mocks {
            debugPrint("EMAIL: \(mock)")
            loginViewModel.email = mock
            loginViewModel.calculateIfIsValidEmail()
            XCTAssertFalse(loginViewModel.isValidEmail)
        }
    }

    /// Testa se os emails válidos pasam os testes
    func testValidEmails() throws {
        let mocks = ["example@example.com",
                     "example@example.co.uk",
                     "example_underscore@example.fr",
                     "example+@example.com",
                     "example@with-hyphen.example.com",
                     "with-hyphen@example.com",
                     "example@1leadingnum.example.com",
                     "1leadingnum@example.com"]

        let loginViewModel = LoginViewModel()

        for mock in mocks {
            debugPrint("EMAIL: \(mock)")
            loginViewModel.email = mock
            loginViewModel.calculateIfIsValidEmail()
            XCTAssertTrue(loginViewModel.isValidEmail)
        }
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
